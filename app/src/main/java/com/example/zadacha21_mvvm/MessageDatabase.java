package com.example.zadacha21_mvvm;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * This is Message database class
 */
@Database(entities = {MessageEntity.class}, version = 1)
public abstract class MessageDatabase extends RoomDatabase {

    private static MessageDatabase instanse;

    public abstract MessageDao messageDao();

    /**
     * This method creates database ONLY ones and return instanse of it.. If database is already created, this method only return instance on it.
     *
     * @param context
     * @return instance of database.
     */
    public static synchronized MessageDatabase getInstance(Context context) {
        if (instanse == null) {
            instanse = Room.databaseBuilder(context.getApplicationContext(),
                    MessageDatabase.class, "contact_databese")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instanse;


    }

}
