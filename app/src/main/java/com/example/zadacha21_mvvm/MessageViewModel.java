package com.example.zadacha21_mvvm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * View Model class
 * implementation all functions from repository
 */
public class MessageViewModel extends AndroidViewModel {
    MessageRepository messageRepository;

    /**
     * Constructor for the class
     *
     * @param application
     */
    public MessageViewModel(@NonNull Application application) {
        super(application);
        this.messageRepository = new MessageRepository(application);
    }

    /**
     * This method gets message from server and insert their to database
     */
    public void updateMessagesFromServer() {
        messageRepository.getMessagesFromServer();
    }

    /**
     * This method gets all messages from database
     *
     * @return list of messages
     */
    public LiveData<List<MessageEntity>> getAllMessages() {
        return messageRepository.getAllMessages();
    }

    /**
     * This method finds messages in database by entering userID
     *
     * @param sortingUserId entering userID for sorting
     * @return list of messages with searching userID
     */
    public LiveData<List<MessageEntity>> findMessageByUserId(String sortingUserId) {
        return messageRepository.findMessagesByUserId(sortingUserId);
    }
}
