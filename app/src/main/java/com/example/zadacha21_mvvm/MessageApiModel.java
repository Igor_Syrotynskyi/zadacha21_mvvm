package com.example.zadacha21_mvvm;

import com.google.gson.annotations.SerializedName;

/**
 * This class describes messages from the server
 */
public class MessageApiModel {

    private int userId;
    private Integer id;
    private String title;

    @SerializedName("body") // Because the key in JSON is called "body" and we push it into "text"
    private String text;

    public Integer getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
}
