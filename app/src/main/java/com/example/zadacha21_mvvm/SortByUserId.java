package com.example.zadacha21_mvvm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import androidx.appcompat.app.AppCompatActivity;

public class SortByUserId extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_USER_ID = "package com.example.zadacha21_mvvm.EXTRA_USER_ID"; //key variable for passing userId to  MainActivity
    private NumberPicker numberPickerUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sort_by_user_id);

        findLayoutElements();

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        //press search by User ID Button
        if (v.getId() == R.id.chooseUserIdButton) {
            // get user ID
            String sortingUserId = chooseUserId();
            intent.putExtra(EXTRA_USER_ID, sortingUserId);
            //sent data to main activity
            setResult(RESULT_OK, intent);
            finish();
        }

    }

    /**
     * This method user ID for sorting
     * @return userID
     */
    public String chooseUserId() {
        int intChoosedUserId = numberPickerUserId.getValue();
        String choosedUserId = String.valueOf(intChoosedUserId);
        return choosedUserId;
    }
    /**
     * This method creates synchronization with all Views of sort_by_user_id Layout.
     */
    public void findLayoutElements(){
        numberPickerUserId = findViewById(R.id.number_picker_user_id);
        numberPickerUserId.setMinValue(1);
        numberPickerUserId.setMaxValue(100);

        Button chooseUserIdButton = findViewById(R.id.chooseUserIdButton);
        chooseUserIdButton.setOnClickListener(this);
    }
}
