package com.example.zadacha21_mvvm;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MessageDao {

    //insert message to database
    @Insert
    void insertMessagesCameFromServer(List<MessageEntity> messageEntityToDatabaseFromServer);

    //get all messages from database
    @Query("SELECT * FROM message_table")
    LiveData<List<MessageEntity>> getAllMessages();

    //get messages from database by sorting userID
    @Query("SELECT * FROM message_table WHERE userID LIKE :sortingUserId")
    LiveData<List<MessageEntity>> findMessagesByUserId(String sortingUserId);
}
