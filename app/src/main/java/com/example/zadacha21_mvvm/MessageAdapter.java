package com.example.zadacha21_mvvm;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for RecyclerView class
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    List<MessageEntity> messages = new ArrayList<>();

    /**
     * This method get list of messages which should to show in Recycler View
     * @param messages - list of contacts
     */
    public void setMessages(List<MessageEntity> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    /**
     * This method find layout with how should to view holder
     * @param parent
     * @param viewType
     * @return - holder
     */
    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.message_item, parent, false);
        return new MessageHolder(itemView);
    }

    /**
     * This method calls every time when user scroll the list of messages and refresh information on screen(set text in views from item layout)
     *
     * @param holder   - View Holder
     * @param position - where will be insert View Holder
     */
    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        MessageEntity currentMessage = messages.get(position);
        holder.idTextView.setText("ID: " + currentMessage.getId());
        holder.userIdTextView.setText("User ID: " + currentMessage.getUserId());
        holder.titleTextView.setText("Title: " + currentMessage.getTitle());
        holder.textTextView.setText("Text: " + currentMessage.getText());

    }

    /**
     * This method gets sum of items
     *
     * @return - amount of items
     */
    @Override
    public int getItemCount() {
        if (messages == null)
            return 0;
        else
            return messages.size();
    }

    /**
     * This class implements how to should look the View Holder
     */
    static class MessageHolder extends RecyclerView.ViewHolder {
        private TextView idTextView;
        private TextView userIdTextView;
        private TextView titleTextView;
        private TextView textTextView;

        /**
         * Constructor of class MessageHolder
         *
         * @param itemView - item layout
         */
        public MessageHolder(@NonNull View itemView) {
            super(itemView);
            idTextView = itemView.findViewById(R.id.idTextView);
            userIdTextView = itemView.findViewById(R.id.userIdTextView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            textTextView = itemView.findViewById(R.id.textTextView);
        }
    }
}
