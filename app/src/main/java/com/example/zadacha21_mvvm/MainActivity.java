package com.example.zadacha21_mvvm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private MessageViewModel messageViewModel;
    private MessageAdapter messageAdapter;
    public static final int SORT_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializationAdapter();
        viewModelObservers();
        findLayoutElements();
    }

    /**
     * This method initializes recyclerview and adapter
     */
    private void initializationAdapter() {
        messageAdapter = new MessageAdapter();
        RecyclerView recyclerView = findViewById(R.id.messagesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(messageAdapter);
    }

    /**
     *This method initializes ViewModel variable and created Live Data observer to message database
     */
    private void viewModelObservers() {
        messageViewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication())).get(MessageViewModel.class);
        messageViewModel.getAllMessages().observe(this, new Observer<List<MessageEntity>>() {
            @Override
            public void onChanged(List<MessageEntity> messages) {
                //update adapter
                messageAdapter.setMessages(messages);
            }
        });
    }

    /**
     * This method creates synchronization with all Views of Main Layout.
     */
    public void findLayoutElements() {
        Button updateButton = findViewById(R.id.updateButton);
        updateButton.setOnClickListener(this);
        Button sortButton = findViewById(R.id.sortByUserIdButton);
        sortButton.setOnClickListener(this);
    }
    /**
     * This method handles the push of a buttons
     *
     * @param v - id of buttons
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updateButton:
                messageViewModel.updateMessagesFromServer();
                break;
            case R.id.sortByUserIdButton:
                Intent intent = new Intent(this, SortByUserId.class);
                startActivityForResult(intent, SORT_REQUEST);
                break;
        }

    }
    /**
     * This method gets results from SortByUserId activity
     *
     * @param requestCode - request code
     * @param resultCode  - result code
     * @param data        - activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // set value of requestCode and resultCode
        super.onActivityResult(requestCode, resultCode, data);
        // if requestCode and resultCode is OK
        if (resultCode == RESULT_OK && requestCode == SORT_REQUEST) {
            //get result from search user ID layout
            String sortingUserId = data.getStringExtra(SortByUserId.EXTRA_USER_ID);
            //get result from DB
            messageViewModel.findMessageByUserId(sortingUserId).observe(this, new Observer<List<MessageEntity>>() {
                @Override
                public void onChanged(List<MessageEntity> sortedMessageEntities) {
                    messageAdapter.setMessages(sortedMessageEntities);
                }
            });
        } else {
            // if requestCode and resultCode is not OK
            Toast.makeText(this, "Messages was not sorted", Toast.LENGTH_SHORT).show();
        }
    }
}
