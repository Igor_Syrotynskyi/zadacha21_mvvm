package com.example.zadacha21_mvvm;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * This class describes a database entity.
 */
@Entity(tableName = "message_table")
public class MessageEntity {

    @PrimaryKey(autoGenerate = true)
    private int position;
    private int userId;
    private Integer id;
    private String title;
    private String text;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
