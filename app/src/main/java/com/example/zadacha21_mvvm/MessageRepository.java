package com.example.zadacha21_mvvm;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Repository class
 * get data from server and insert there to local database
 * implementation all functions which help us work with messages from database
 */
public class MessageRepository {
    private MessageDao messageDao;
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private final String TAG = "RetrofitMessage:";

    /**
     * Constructor for the class where getting instance of database, variable of Dao and LiveData list of messages from database
     *
     * @param application
     */
    public MessageRepository(Application application) {
        createRetrofit();
        MessageDatabase messageDatabase = MessageDatabase.getInstance(application);
        messageDao = messageDatabase.messageDao();
    }

    /**
     * This method initialize the Retrofit variable
     *
     * @return
     */
    private JsonPlaceHolderApi createRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/") //Specify the resource where exactly the data lies.
                .addConverterFactory(GsonConverterFactory.create()) //Specify that you want to extract in GSON format
                .build();
        return jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
    }


    /**
     * This method gets messages from server and save it in messageApiModel variable.
     * Then call method which convert from ApiModel list to Entity list.
     * Then call method which
     */
    public void getMessagesFromServer() {
        //sending request to server
        Call<List<MessageApiModel>> call = jsonPlaceHolderApi.getMessages();
        //creating callback in server
        call.enqueue(new Callback<List<MessageApiModel>>() {
            @Override
            //callback is successful
            public void onResponse(Call<List<MessageApiModel>> call, Response<List<MessageApiModel>> response) {
                //request is not successful
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Code - " + response.code());
                }
                //request is successful. We got messages
                List<MessageApiModel> messageApiModelList = response.body();

                //convert messages from server and set it to database
                List<MessageEntity> messageEntityToDatabaseFromServer = convertingMessagesFromApiModelToEntity(messageApiModelList);

                //insert messages to local database
                insertMessagesCameFromServer(messageEntityToDatabaseFromServer);
            }

            //callback is not successful
            @Override
            public void onFailure(Call<List<MessageApiModel>> call, Throwable t) {
                Log.d(TAG, t.getMessage());  //Show text of error in log
            }
        });
    }

    /**
     * This method got list of messages from server, convert it to MessageEntity list
     *
     * @param messageApiModelList -the list of messages from server
     * @return - converted to MessageEntity list
     */
    public List<MessageEntity> convertingMessagesFromApiModelToEntity(List<MessageApiModel> messageApiModelList) {
        List<MessageEntity> messageEntityList = new ArrayList<>();
        for (MessageApiModel message : messageApiModelList) {
            MessageEntity messageEntity = new MessageEntity();
            messageEntity.setId(message.getId());
            messageEntity.setUserId(message.getUserId());
            messageEntity.setTitle(message.getTitle());
            messageEntity.setText(message.getText());
            messageEntityList.add(messageEntity);
        }
        return messageEntityList;
    }


    /**
     * Insert message to database
     */
    public void insertMessagesCameFromServer(List<MessageEntity> messageEntityToDatabaseFromServer) {
        MessageEntity[] messageEntitiesArray = new MessageEntity[messageEntityToDatabaseFromServer.size()];
        messageEntityToDatabaseFromServer.toArray(messageEntitiesArray);
        new InsertMessagesCameFromServerAsyncTask(messageDao).execute(messageEntitiesArray);
    }

    /**
     * This method get Live Data list with all messages in database
     *
     * @return the List with all messages
     */
    public LiveData<List<MessageEntity>> getAllMessages() {
        return messageDao.getAllMessages();
    }

    /**
     * This method finds all messages in database by userID
     *
     * @param sortingUserId - number of userID which we search
     * @return list of messages with searching ID
     */
    public LiveData<List<MessageEntity>> findMessagesByUserId(String sortingUserId) {
        return messageDao.findMessagesByUserId(sortingUserId);
    }


    private static class InsertMessagesCameFromServerAsyncTask extends AsyncTask<MessageEntity, Void, Void> {
        private MessageDao messageDao;

        private InsertMessagesCameFromServerAsyncTask(MessageDao messageDao) {
            this.messageDao = messageDao;
        }

        @Override
        protected Void doInBackground(MessageEntity... messageEntitiesArray) {
            messageDao.insertMessagesCameFromServer(Arrays.asList(messageEntitiesArray));
            return null;
        }
    }
}
