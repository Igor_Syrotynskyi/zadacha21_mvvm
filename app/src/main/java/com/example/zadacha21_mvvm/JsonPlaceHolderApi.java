package com.example.zadacha21_mvvm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * This interface implements requests to the server
 */
public interface JsonPlaceHolderApi {
    //get messages from server
    @GET("posts")
    // 'posts', because the information we need lies on the server, with local addressing 'posts'
    Call<List<MessageApiModel>> getMessages();

}
